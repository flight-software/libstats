#ifndef LIB_STATS_H
#define LIB_STATS_H
/**
 * \file libstats.h
 * \brief General number collection and exporting
 *
 * Useful for benchmarking and libserial stuff
 *
 * Internally this is just a dll_fs containing an array of numbers, and
 * we write that to a CSV upon request for some better parsing
 *
 * We use quadmath, since that's cool now
 *
 * TODO: allow for circular buffer for time critical stuff
 */

#include <stdint.h>

#include "libdll.h"

typedef uint32_t stats_p_val_fs;

/**
 * \brief How many iterations of the Taylor series do we run?
 *
 * If you drive this number too high, you eventually end up with NaN
 *
 * This seems to be a good compromise
 */
#define STAT_NORMAL_TAYLOR_NUMBER 128

struct stats_fs {
    dll_fs list; //< List containing samples
    size_t elem_size; //< Length of elements in list, 1, 2, 4, or 8
    size_t elem_count; //< Number of elements in a dll_fs entry
    size_t max_size; //< Max size of underlying dll
    size_t curr_size; //< Current number of elements in dll
    char* path; //< Associated path to write CSV to
};

typedef struct stats_fs stats_fs;

int stats_init(stats_fs* stats, const char* path, size_t max_size, size_t elem_size, size_t elem_count);
int stats_add(stats_fs* stats, const void* data);
int stats_calc(stats_fs* stats, long double* mean, long double* std_dev, size_t len);
int stats_export(stats_fs* stats);
int stats_close(stats_fs* stats);

size_t stats_sample_size(const stats_fs* stats);
stats_p_val_fs stats_normal_two_sample_z(long double first_mean, long double first_std_dev, size_t first_pop,
                                        long double second_mean, long double second_std_dev, size_t second_pop);
long double stats_normal_box_muller(long double mean, long double std_dev);
uint64_t stats_factorial(uint64_t a);

uint8_t stats_fsest_pass(stats_p_val_fs p_val, long double crit_val);

#define PVAL_ONE ((long double)(1ULL << sizeof(stats_p_val_fs)*8))-1.0L

#define PVAL_TO_FLOAT(x___) ((long double)x___) * ((long double)((1ULL << (sizeof(stats_p_val_fs)*8)) - 1))

#define FLOAT_TO_PVAL(x___) ((long double)x___) / ((long double)((1ULL << (sizeof(stats_p_val_fs)*8)) - 1))

#define MAX_IN_ARRAY(array___, len___, pos_var___)  \
    if(true){                                       \
        pos_var___ = 0;                             \
        for(size_t i = 0;i < len___;i++){           \
            if(array___ [pos_var___] < array___ [i]){\
                pos_var___ = i;                      \
            }                                        \
        }                                            \
    }                                               \
    

#define MIN_IN_ARRAY(array___, len___, pos_var___)  \
    if(true){                                       \
        pos_var___ = 0;                             \
        for(size_t i = 0;i < len___;i++){           \
            if(array___ [pos_var___] < array___ [i]){\
                pos_var___ = i;                      \
            }                                        \
        }                                            \
    }                                               \
    
#endif
