#include <stdint.h>
#include <inttypes.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>

#include "libdebug.h"
#include "libdll.h"
#include "libstats.h"

/**
 * \file libstats.c
 * \brief Stats implementation, basically just logging at this point
 */

/**
 * \brief Pull element of a varying size array and cast it up
 *
 * \param[in] data 				Array to pull data from
 * \param[in] elem_len			Length of element (1, 2, 4, 8)
 * \param[in] elem_pos			Position of element
 *
 * \return Element size
 */

static int64_t stats_pull_data_from_size(void* data, size_t elem_len, size_t elem_pos)
{
    switch(elem_len) {
    case 1:
        return (((int8_t*)data)[elem_pos]);
    case 2:
        return (((int16_t*)data)[elem_pos]);
    case 4:
        return (((int32_t*)data)[elem_pos]);
    case 8:
        return (((int64_t*)data)[elem_pos]);
    default:
        P_ERR_STR("invalid size");
    }
    return 0;
}

/**
 * \brief Initialize stats structure
 *
 * If you don't need a CSV file backing, set path to NULL
 *
 * \param[in] stats				Uninitialized stats structure
 * \param[in] path				Path of CSV to write, can be NULL
 * \param[in] max_size			Max number of elements to keep
 * \param[in] elem_size			Size of element in array of data to parse (1, 2, 4 or 8)
 * \param[in] elem_count		Number of elements per sample
 * 
 * \return EXIT_FAILURE on failure, EXIT_SUCCESS otherwise
 */
int stats_init(stats_fs* stats, const char* path, size_t max_size, size_t elem_size, size_t elem_count)
{
    if(!stats) {
        P_ERR_STR("stat is null");
        return EXIT_FAILURE;
    }

    memset(stats, 0, sizeof(stats_fs));

    if(path) {
        stats->path = strdup(path);
    }

    if(dll_init(&(stats->list), NULL)) return EXIT_FAILURE;
    
    stats->elem_size = elem_size;
    stats->elem_count = elem_count;
    stats->max_size = max_size;

    return EXIT_SUCCESS;
}

/**
 * \brief Add elements to stats
 *
 * Data must be elem_size*elem_count long, and generally be sane
 *
 * \param[in] stats				Stats structure
 * \param[in] data				Data to log with stats structure
 *
 * \return EXIT_FAILURE on failure, EXIT_SUCCESS otherwise
 */

int stats_add(stats_fs* stats, const void* data)
{
    if(!stats) {
        P_ERR_STR("log is null");
        return EXIT_FAILURE;
    }
    
    if(!data) return EXIT_FAILURE;
    
    const size_t payload_size = stats->elem_size * stats->elem_count;
    void* dll_payload = malloc(payload_size);
    
    if(!dll_payload) {
        P_ERR("malloc failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    
    memcpy(dll_payload, data, payload_size);

    if(stats->curr_size == stats->max_size) {
        dll_pull_head(&(stats->list));
        stats->curr_size--;
    }

    if(dll_push_tail(&(stats->list), dll_payload, payload_size, 0)) {
        free(dll_payload);
        P_ERR_STR("Failed to push element");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

/**
 * \brief Callback for dll_search
 *
 * \param[in] data				Current dll_fs element
 * \param[in] param				Cast to void*[2], FILE* and stats_fs
 *
 * \return Always zero
 */
static uint8_t stats_export_dll(void* data, void* param)
{
    void** params = (void**)param;

    FILE* file = params[0];
    
    const stats_fs* stats = params[1];

    if(!file || !stats) {
        return 0;
    }

    // INT64_MAX has 20 digits
    char* buffer = malloc(21 * stats->elem_count);

    if(!buffer) {
        P_ERR("Failed to malloc %zu bytes, errno: %d (%s)", 21 * stats->elem_count, errno, strerror(errno));
        return 0;
    }

    char tmp[64];
    buffer[0] = 0;
    for(size_t i = 0; i < stats->elem_count; i++) {
        tmp[0] = 0;
        const int64_t tmp_int = stats_pull_data_from_size(data, stats->elem_size, i);
        sprintf(tmp, "%"PRIi64",", tmp_int);
        strcat(buffer, tmp);
    }
    // replace last comma with a newline
    buffer[strlen(buffer)-1] = '\n';

    if(fputs(buffer, file) == EOF) {
        P_ERR("Failed to append buffer (%s) to file, errno: %d (%s)", buffer, errno, strerror(errno));
    }

    free(buffer);

    return 0;
}

/**
 * \brief Generate a rolling average
 *
 * \param[in] data				Array of elements
 * \param[in] param				void*[4]
 *
 * \return Zero
 */
static uint8_t stats_calc_mean_cb(void* data, void* param)
{
    void** param_list = (void**)param;

    const stats_fs* stats = param_list[0];

    long double* means = param_list[1];
    
    uint64_t* cur_pos = param_list[2];
    
    for(size_t i = 0; i < stats->elem_count; i++) {
        const long double tmp_data = (long double)stats_pull_data_from_size(data, stats->elem_size, i);
        means[i] = (means[i] + tmp_data) / (long double)(*cur_pos);
    }

    (*cur_pos)++;
    
    return 0;
}

/**
 * \brief Generate standard deviations
 *
 * \param[in] data				Array of elements
 * \param[in] param				void*[4]
 *
 * \return Zero
 */
static uint8_t stats_calc_std_dev_cb(void* data, void* param)
{
    void **param_list = (void**)param;

    const stats_fs* stats = param_list[0];

    long double* std_devs = param_list[1];
    
    const long double* means = param_list[2];
    
    uint64_t* cur_pos = param_list[3];

    for(size_t i = 0; i < stats->elem_count; i++) {
        const long double tmp_data = (long double)stats_pull_data_from_size(data, stats->elem_size, i);
        std_devs[i] = (std_devs[i] + (fabsl(means[i] - tmp_data))) / (long double)(*cur_pos);
    }

    (*cur_pos)++;
    
    return 0;
}

/**
 * \brief Calculate some simple statistics
 *
 * \param[in] stats				Stats structure
 * \param[out] mean				Calculated mean, length of number of elements per sample
 * \param[out] std_dev			Calculated std dev, length of number of elements per sample
 * 
 * \return EXIT_FAILURE on failure, EXIT_SUCCESS otherwise
 */
int stats_calc(stats_fs* stats, long double* mean, long double* std_dev, size_t len)
{
    if(!stats) {
        P_ERR_STR("stats is null");
        return EXIT_FAILURE;
    }

    if(mean || std_dev) {
        memset(mean, 0, len * sizeof(long double));
        
        uint64_t cur_pos = 1;
        void* calc_param[4];

        calc_param[0] = stats;
        calc_param[1] = mean;
        calc_param[2] = &cur_pos;
        calc_param[3] = NULL;
        
        dll_search(&(stats->list), stats_calc_mean_cb, calc_param);
    }

    if(std_dev) {
        memset(std_dev, 0, len * sizeof(long double));
        
        uint64_t cur_pos = 1;
        void* calc_param[4];
        
        calc_param[0] = stats;
        calc_param[1] = std_dev;
        calc_param[2] = mean;
        calc_param[3] = &cur_pos;
        
        dll_search(&(stats->list), stats_calc_std_dev_cb, calc_param);
    }

    return EXIT_SUCCESS;
}

/**
 * \brief Commit stats to CSV
 *
 * CSV path is given at initialization
 *
 * \param[in] stats				Stats structure
 *
 * \return EXIT_FAILURE on failure, EXIT_SUCCESS otherwise
 */
int stats_export(stats_fs* stats)
{
    if(!stats) {
        P_ERR_STR("stats is null");
        return EXIT_FAILURE;
    }

    if(!stats->path) {
        P_ERR_STR("No path to make CSV");
        return EXIT_FAILURE;
    }
    
    FILE* file_ptr = fopen(stats->path, "w+");
    
    if(!file_ptr) {
        P_ERR("Failed to open file (%s), errno: %d (%s)", stats->path, errno, strerror(errno));
        return EXIT_FAILURE;
    }
    
    void* cb_payload[2];
    cb_payload[0] = file_ptr;
    cb_payload[1] = stats;

    dll_search(&(stats->list), stats_export_dll, cb_payload);

    fclose(file_ptr);

    return EXIT_SUCCESS;
}

/**
 * \brief Close stats structure
 *
 * This function doesn't automatically commit a CSV to disk
 *
 * \param[in] stats				Stats structure
 *
 * \return EXIT_FAILURE on failure, EXIT_SUCCESS otherwise
 */
int stats_close(stats_fs* stats)
{
    if(!stats) {
        P_ERR_STR("stats is null");
        return EXIT_FAILURE;
    }

    free(stats->path);

    if(dll_close(&(stats->list))) return EXIT_FAILURE;

    return EXIT_SUCCESS;
}

/**
 * \brief Pull current sample size
 *
 * As all array elements progress in tandem, this applies to all of them
 * 
 * \param[in] stats				Stats structure
 * 
 * \return Sample size
 */
size_t stats_sample_size(const stats_fs* stats)
{
    return stats->list.cur_size;
}

/**
 * \brief Two Sample Z Test
 *
 * Operates on a Taylor Series expansion of the integral of 
 * (2*pi)^(-1/2) *e^(-(x^2)/2) (standard normal)
 *
 * TODO: Create a macro that expands out to the Nth entry in the Taylor series,
 * and just add those together in tandem, so we remove the bloat of having to
 * compute constant values on the fly
 *
 * \param[in] first_mean		First mean
 * \param[in] first_std_dev		First standard deviation
 * \param[in] first_pop			First population
 * \param[in] second_mean		Second mean
 * \param[in] second_std_dev	Second standard deviation
 * \param[in] second_pop		Second population
 *
 * \return Percent confidence in a match (H0 is x1 != x2)
 */

stats_p_val_fs stats_normal_two_sample_z(long double first_mean, long double first_std_dev, size_t first_pop,
                                   long double second_mean, long double second_std_dev, size_t second_pop)
{
    const long double first_half_pre_root = (first_std_dev * first_std_dev) / first_pop;
    const long double second_half_pre_root = (second_std_dev * second_std_dev) / second_pop;
    const long double z_score = (first_mean - second_mean) / sqrtl(first_half_pre_root + second_half_pre_root);
    
    long double norm_approx = 0;
    for(size_t i = 0; i < STAT_NORMAL_TAYLOR_NUMBER; i++) {
        const long double num = powl(z_score, (2.0L * i) + 1.0L);
        const long double fact = (long double)stats_factorial(i);
        const long double int_denom = (2.0L * i) + 1.0L;
        const long double pow_denom = powl(2.0L, i);
        if(i % 2) {
            norm_approx += num / (pow_denom * fact * int_denom);
        } else {
            norm_approx -= num / (pow_denom * fact * int_denom);
        }
    }
    // 2/sqrt(2*M_PI)
    norm_approx *= 0.7978845608028653558798921198687637369517172623298693153318516593413158517993921407993139575490435934L;

    if(norm_approx > 1.0L) norm_approx = 1.0L;
    const long double scale_factor = (long double)(1ULL << sizeof(stats_p_val_fs) * 8) - 1.0L;
    return (stats_p_val_fs)((norm_approx * scale_factor) + 0.5L);
}

/**
 * \brief Box-Muller transform for generating gaussian noise
 *
 * \return EXIT_FAILURE on failure, EXIT_SUCCESS otherwise
 */

long double stats_normal_box_muller(long double mean, long double std_dev)
{
    static long double z_1;
    static bool generate;
    
    generate = !generate;
    
    if(!generate) {
        return (z_1 * std_dev) + mean;
    }

    long double u_1, u_2;
    do {
        u_1 = rand() * (1.0L / RAND_MAX);
        u_2 = rand() * (1.0L / RAND_MAX);
    } while(u_1 <= 0.000000001L);

    long double z_0;
    z_0 = sqrtl(-2.0L * logl(u_1)) * cosl((2 * M_PI) * u_2);
    z_1 = sqrtl(-2.0L * logl(u_1)) * sinl((2 * M_PI) * u_2);
    
    return z_0 * std_dev + mean;
}

/**
 * \brief Compute factorial
 */
uint64_t stats_factorial(uint64_t a)
{
    uint64_t retval = a;
    while(a != 0) {
        retval *= (a - 1);
        a--;
    }
    return a;
}

/**
 * \brief Determine if a P-value passes or fails given a critical value
 *
 * NOTE: double check stats book to make sure nothing I wrote is broken
 */
uint8_t stats_fsest_pass(stats_p_val_fs p_val, long double crit_val)
{
    return (1- PVAL_TO_FLOAT(p_val)) < crit_val;
}
